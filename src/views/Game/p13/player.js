/*

玩家控制

*/
import {winw, winh} from './config'
import MusicController from '../public/MusicController'
import State from './state'

const PlayerController = {
    // phaser对象
    phaser: null,


    // 初始化挂载phaser对象
    init: function (phaser) {
        this.phaser = phaser
        this.resetUser()
    },

    // 充重置用户数据
    resetUser: function () {
        this.players = []
        for (let i = 0; i < 13; i++) {
            this.players.push(new Player())
        }
    },

    // 更新所有用户
    refresh: function () {
        let vm = this
        vm.players.forEach((e, i) => {
            vm.update(i)
        })
    },

    update: function (index = 0) {
        let vm = this
        let game = vm.phaser.game
        const w = winw / 100
        const h = winh / 100
        let player = vm.players[index]

        let offseTop = 0
        let offseLeft = 4 * w
        // 左边一排
        if (index <= 6) {
            offseTop = 1 * h + (12.4 * (6 - index) * h)
        }
        // 右边一排
        else {
            offseTop = 1 * h + (12.4 * (index - 7) * h)
            offseLeft = winw - 16 * w
        }

        // 玩家自己
        if (index == 0) {
            offseTop = 1 * h + (12.4 * (6.5 - index) * h)
            offseLeft = 2 * w
        }

        if (player.status) { // 有用户

            if (player.status == 1) {
                /* 头像 */
                if (player.avatar) {
                    // 头像
                    player.avatar = game.add.sprite(offseLeft + 8, offseTop, "advatar_default")
                    player.avatar.scale.setTo((12 * w - 16) / player.avatar.width, (6.8 * h - 16) / player.avatar.height);
                    try {
                        player.avatar = game.add.sprite(offseLeft + 8, offseTop, "avatar" + index)
                        player.avatar.scale.setTo((12 * w - 16) / player.avatar.width, (6.8 * h - 16) / player.avatar.height);
                    } catch {
                    }
                } else {
                    player.avatar = game.add.sprite(offseLeft + 8, offseTop, "advatar_default")
                    player.avatar.scale.setTo((12 * w - 16) / player.avatar.width, (6.8 * h - 16) / player.avatar.height);
                    try {
                        // 动态加载
                        let loader = new Phaser.Loader(game);  //game为你当前的游戏实例
                        loader.crossOrigin = 'anonymous'; // 设置跨域
                        loader.image("avatar" + index, player.info.avatar); //加载一张图片 player.info.avatar
                        loader.onLoadComplete.add(function(){ //加载完成事件
                            // 头像
                            player.avatar = game.add.sprite(offseLeft + 8, offseTop, "avatar" + index)
                            player.avatar.scale.setTo((12 * w - 16) / player.avatar.width, (6.8 * h - 16) / player.avatar.height);
                        });
                        loader.start() //开始加载
                    } catch {
                        console.log('头像加载失败！')
                    }
                }
                setTimeout(() => {
                    if (player.ready) {
                        player.readyDom = game.add.text(offseLeft + 8, offseTop, '已准备',  { font: "24px Arial", fill: '#FFCC00'});     
                        player.readyDom.stroke = '#CC0000';   
                        player.readyDom.strokeThickness = 4;            
                    } else {
                        player.readyDom && player.readyDom.destroy()
                    }
                }, 300)
            } else {
                player.avatarBorder = game.add.sprite(offseLeft, offseTop, 'advatar_offline');
                player.avatarBorder.scale.setTo(12 * w / player.avatarBorder.width, 6.8 * h / player.avatarBorder.height);
            }
             
            
            // 头像框
            player.avatarBorder = game.add.sprite(offseLeft, offseTop - 10, player.info.avatarBorder);
            player.avatarBorder.scale.setTo(12 * w / player.avatarBorder.width, 11.4 * h / player.avatarBorder.height);
            // 名字
            let style = { font: "24px Arial", fill: "#ffffff", align: "center" };
            player.userName = game.add.text(offseLeft + 6 * w, offseTop + (6.8 * h) + 8, player.info.userName, style);
            player.userName.scale.setTo(0.8, 0.8)
            player.userName.anchor.set(0.5);

            // 积分
            let style2 = { font: "32px Arial", fill: "#FF9000", align: "center" };
            player.score = game.add.text(offseLeft + 6 * w, offseTop + (6.8 * h) + 36, player.info.score, style2);
            player.score.anchor.set(0.5);
        } else {
            /* 头像框 */
            player.avatarBorder = game.add.sprite(offseLeft, offseTop, 'advatar_null');
            player.avatarBorder.scale.setTo(12 * w / player.avatarBorder.width, 6.8 * h / player.avatarBorder.height);
        }

    },

    // 玩家列表
    players: [],

    // 给一个玩家播报牛牛
    // index 哪个玩家 -1 为当前用户
    // val 牛牛语音值
    showNiuNiu: function (index = 0, val = 60) {
        let vm = this
        let game = vm.phaser.game
        let voice = 'sound_1_' + val
        MusicController.target[voice].play()
        let colors = {
            60: ['#eeeeee', '#cccccc', '没牛'],
            61: ['#F8D800', '#FCFF00', '牛一'],
            62: ['#F8D800', '#FCFF00', '牛二'],
            63: ['#F8D800', '#FCFF00', '牛三'],
            64: ['#F8D800', '#FCFF00', '牛四'],
            65: ['#F8D800', '#FCFF00', '牛五'],
            66: ['#F8D800', '#FCFF00', '牛六'],
            67: ['#F8D800', '#FCFF00', '牛七'],
            68: ['#F8D800', '#FCFF00', '牛八'],
            69: ['#F8D800', '#FCFF00', '牛九'],
            70: ['#FDC830', '#F37335', '牛牛'],
            71: ['#FFF886', '#F072B6', '花牛'],
            72: ['#FFF886', '#F072B6', '炸弹牛'],
            73: ['#FFF886', '#F072B6', '五小牛'],
            74: ['#FFF886', '#F072B6', '金牛'],
            161: ['#FDD819', '#E80505', '地牛'],
            162: ['#FDD819', '#E80505', '天牛'],
            163: ['#FDD819', '#E80505', '顺子牛'],
            171: ['#FDD819', '#E80505', '同花牛'],
            172: ['#FDD819', '#E80505', '葫芦牛'],
            801: ['#F8D800', '#FCFF00', '一倍'],
            802: ['#F8D800', '#FCFF00', '二倍'],
            803: ['#F8D800', '#FCFF00', '三倍'],
            804: ['#F8D800', '#FCFF00', '四倍'],
            805: ['#F8D800', '#FCFF00', '五倍'],
            808: ['#F8D800', '#FCFF00', '八倍'],
            810: ['#F8D800', '#FCFF00', '十倍'],
            98: ['#F8D800', '#FCFF00', '抢庄'],
            99: ['#F8D800', '#FCFF00', '不抢'],
        }
        const w = winw / 100
        const h = winh / 100

        let offseTop = 0
        let offseLeft = 17 * w
        // 左边一排
        if (index <= 6) {
            offseTop = 1 * h + (12.4 * (6 - index) * h)
        }
        // 右边一排
        else {
            offseTop = 1 * h + (12.4 * (index - 7) * h)
            offseLeft = winw - 18 * w
        }

        // 玩家自己
        if (index == 0) {
            offseTop = 1 * h + (12.4 * (6.5 - index) * h)
            offseLeft = 14 * w
        }

        let text = game.add.text(offseLeft, offseTop, colors[val][2]);
        if (index > 6) {
            text.anchor.set(1, 0);
        }
        if (index < 0) {
            text.anchor.set(0.5, 0);
        }
        text.fontWeight = 'bold';
        text.fontSize = 50;
        if (index < 0) {
            text.fontSize = 80;
        }
        text.stroke = '#000000';
        text.strokeThickness = 2;
        let grd = text.context.createLinearGradient(0, 0, 0, text.height);
        grd.addColorStop(0, colors[val][0]);   
        grd.addColorStop(1, colors[val][1]);
        text.fill = grd;
        game.add.tween(text).from( { width: 0, height: 0 }, 500, Phaser.Easing.Bounce.Out, true);

        setTimeout(() => {
            text.destroy();
        }, 2000)
    },

    // 给一个玩家发牌
    pokerPlayer: function (index = 0) {
        let vm = this
        let game = vm.phaser.game
        const w = winw / 100
        const h = winh / 100

        let offseTop = 0
        let offseLeft = 17 * w
        // 左边一排
        if (index <= 6) {
            offseTop = 1 * h + (12.4 * (6 - index) * h)
        }
        // 右边一排
        else {
            offseTop = 1 * h + (12.4 * (index - 7) * h)
            offseLeft = winw - 18 * w
        }

        // 玩家自己
        if (index == 0) {
            offseTop = 1 * h + (12.4 * (6.5 - index) * h)
            offseLeft = 22 * w
        }
        let poker_group = game.add.group();
        MusicController.target.sound_101.play();
        for (let i = 0; i < 5; i++) {
            setTimeout(() => {
                let left = offseLeft
                if (index <= 6) {
                    left = offseLeft + i * 4 * w
                } else {
                    left = offseLeft - i * 4 * w
                }
                if (index == 0) {
                    left = offseLeft + i * 14 * w
                }
                let poker = game.add.sprite(left, offseTop, 'c_' + 54);
                if (index == 0) {
                    poker.scale.setTo(1.6, 1.6)
                } else {
                    poker.scale.setTo(1.3, 1.3)
                }
                if (index > 6) {
                    poker.anchor.set(1, 0);
                }
                poker_group.add(poker)
            }, 100 * i)
        }
        PlayerController.players[index].pokers = poker_group;
    },

     // 给一个玩家开牌
     showPoker: function (index = 0, arr = [52,52,52,52,52]) {
        let vm = this
        let game = vm.phaser.game
        const w = winw / 100
        const h = winh / 100

        let offseTop = 0
        let offseLeft = 17 * w
        // 左边一排
        if (index <= 6) {
            offseTop = 1 * h + (12.4 * (6 - index) * h)
        }
        // 右边一排
        else {
            offseTop = 1 * h + (12.4 * (index - 7) * h)
            offseLeft = winw - 34 * w
        }

        // 玩家自己
        if (index == 0) {
            offseTop = 1 * h + (12.4 * (6.5 - index) * h)
            offseLeft = 22 * w
        }
        let poker_group = game.add.group();
        if (PlayerController.players[index] && PlayerController.players[index].pokers) {
             PlayerController.players[index].pokers.destroy();
        }
        arr.forEach((e, i) => {
            let left = offseLeft
            left = offseLeft + i * 4 * w
            if (index ==0) {
                left = offseLeft + i * 14 * w
            }
            let poker = game.add.sprite(left, offseTop, 'c_' + e);
            if (index == 0) {
                poker.scale.setTo(1.6, 1.6)
            } else {
                poker.scale.setTo(1.3, 1.3)
            }
            if (index > 6) {
                poker.anchor.set(1, 0);
            }
            poker_group.add(poker)
        })
        PlayerController.players[index].pokers = poker_group;
    },

    // 转换牌面
    transPoker: function (str) {
        // 11 => 2 12 => 2 + 4
        let color = str[0]
        let size = str[1]
        let colorIndex = 0
        switch (color) {
            case '1': // 黑
                colorIndex = 2
                break
            case '2': // 红
                colorIndex = 0
                break
            case '3': // 梅
                colorIndex = 3
                break
            case '4': // 方
                colorIndex = 1
                break
            case '5': // 方
                colorIndex = 2
                break
        }
        let sizeIndex = 0
        switch (size) {
            case 'a': // 10
                sizeIndex = 9
                break
            case 'b': // J
                sizeIndex = 10
                break
            case 'c': // Q
                sizeIndex = 11
                break
            case 'd': // K
                sizeIndex = 12
                break
            default:
                sizeIndex = Number(size) -1
        }
        return sizeIndex * 4 + colorIndex
    }
    

}

class Player {
    constructor () {
        // 状态  0 => 空缺    1 => 在线   2 => 离开
        this.status = 0,
        // id
        this.id = '',
        // 头像框
        this.avatarBorder = null,
        // 头像
        this.avatar = null,
        // 名字
        this.userName = null,
        // 积分
        this.score = null,
        // 准备信息
        this.readyDom = null,
        // 准备状态
        this.ready = false,
        // 用户信息
        this.info = {
            avatarBorder: 'advatar_user',
            avatar: '',
            userName: '加载中...',
            score: '0'
        },
        // 手牌
        this.pokers = null
    }
}

export default PlayerController;