/*

音乐控制

*/

const MusicController = {
    // phaser对象
    phaser: null,

    // 初始化挂载phaser对象
    init: function (phaser) {
        this.phaser = phaser
    },

    // 全局音效开关
    play: true,

    // 所有音频
    allMusic: [
        // {name: 'edge',  src: 'static/music/00edge.mp3'}, // 测试音乐
        {name: 'bg_music',  src: 'static/music/bgm.mp3'}, // 背景音乐
        // 语音消息
        {name: 'sound_1_0', src: 'static/music/sound_1_0.mp3', value: 0, text: '快点吧，我等到花儿也谢了'},
        {name: 'sound_1_1', src: 'static/music/sound_1_1.mp3', value: 1, text: '我出去叫人'},
        {name: 'sound_1_2', src: 'static/music/sound_1_2.mp3', value: 2, text: '你的牌好靓哇'},
        {name: 'sound_1_3', src: 'static/music/sound_1_3.mp3', value: 3, text: '我当年横扫澳门九条街'},
        {name: 'sound_1_4', src: 'static/music/sound_1_4.mp3', value: 4, text: '算你牛逼'},
        {name: 'sound_1_5', src: 'static/music/sound_1_5.mp3', value: 5, text: '别吹牛逼，有本事干到底'},
        {name: 'sound_1_6', src: 'static/music/sound_1_6.mp3', value: 6, text: '输得裤衩都没了'},
        {name: 'sound_1_7', src: 'static/music/sound_1_7.mp3', value: 7, text: '我给你们送温暖了'},
        {name: 'sound_1_8', src: 'static/music/sound_1_8.mp3', value: 8, text: '谢谢老板'},
        {name: 'sound_1_9', src: 'static/music/sound_1_9.mp3', value: 9, text: '我来啦，让你们久等了'},
        // 牛牛信息
        {name: 'sound_1_60', src: 'static/music/sound_1_60.mp3', value: 60, info: '没牛'},
        {name: 'sound_1_61', src: 'static/music/sound_1_61.mp3', value: 61, info: '牛一'},
        {name: 'sound_1_62', src: 'static/music/sound_1_62.mp3', value: 62, info: '牛二'},
        {name: 'sound_1_63', src: 'static/music/sound_1_63.mp3', value: 63, info: '牛三'},
        {name: 'sound_1_64', src: 'static/music/sound_1_64.mp3', value: 64, info: '牛四'},
        {name: 'sound_1_65', src: 'static/music/sound_1_65.mp3', value: 65, info: '牛五'},
        {name: 'sound_1_66', src: 'static/music/sound_1_66.mp3', value: 66, info: '牛六'},
        {name: 'sound_1_67', src: 'static/music/sound_1_67.mp3', value: 67, info: '牛七'},
        {name: 'sound_1_68', src: 'static/music/sound_1_68.mp3', value: 68, info: '牛八'},
        {name: 'sound_1_69', src: 'static/music/sound_1_69.mp3', value: 69, info: '牛九'},
        {name: 'sound_1_70', src: 'static/music/sound_1_70.mp3', value: 70, info: '牛牛'},
        {name: 'sound_1_71', src: 'static/music/sound_1_71.mp3', value: 71, info: '花牛'},
        {name: 'sound_1_72', src: 'static/music/sound_1_72.mp3', value: 72, info: '炸弹牛'},
        {name: 'sound_1_73', src: 'static/music/sound_1_73.mp3', value: 73, info: '五小牛'},
        {name: 'sound_1_74', src: 'static/music/sound_1_74.mp3', value: 74, info: '金牛'},
        {name: 'sound_1_161', src: 'static/music/sound_1_161.mp3', value: 161, info: '地牛'},
        {name: 'sound_1_162', src: 'static/music/sound_1_162.mp3', value: 162, info: '天牛'},
        {name: 'sound_1_163', src: 'static/music/sound_1_163.mp3', value: 163, info: '顺子牛'},
        {name: 'sound_1_171', src: 'static/music/sound_1_171.mp3', value: 171, info: '同花牛'},
        {name: 'sound_1_172', src: 'static/music/sound_1_172.mp3', value: 172, info: '葫芦牛'},
        // 倍率
        {name: 'sound_1_801', src: 'static/music/sound_1x1.mp3', value: 801, info: '1倍'},
        {name: 'sound_1_802', src: 'static/music/sound_1x2.mp3', value: 802, info: '2倍'},
        {name: 'sound_1_803', src: 'static/music/sound_1x3.mp3', value: 803, info: '3倍'},
        {name: 'sound_1_804', src: 'static/music/sound_1x4.mp3', value: 804, info: '4倍'},
        {name: 'sound_1_805', src: 'static/music/sound_1x5.mp3', value: 805, info: '5倍'},
        {name: 'sound_1_808', src: 'static/music/sound_1x8.mp3', value: 808, info: '8倍'},
        {name: 'sound_1_810', src: 'static/music/sound_1x10.mp3', value: 810, info: '10倍'},
        // 抢庄
        {name: 'sound_1_98', src: 'static/music/sound_1_98.mp3', value: 98, info: '抢庄'},
        {name: 'sound_1_99', src: 'static/music/sound_1_99.mp3', value: 99, info: '不抢'},
        // 洗牌
        {name: 'sound_101', src: 'static/music/sound_101.mp3', value: 1101, info: '洗牌'},
        // 金币
        {name: 'sound_jinbi', src: 'static/music/sound_jinbi.mp3', value: 901, info: '金币'},
        
    ],

    // 音频实例
    target: {},

    // 加载所有音效
    loadAllMusic: function () {
        let game = this.phaser.game
        let vm = this
        vm.allMusic.forEach(e => {
            vm.target[e.name] = game.add.audio(e.name)
        })
    }

}

export default MusicController