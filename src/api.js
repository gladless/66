import http from './http'


const TetBaseUrl = 'http://120.79.184.105:9000'
 const UserBaseUrl = 'http://120.79.184.105:9002'
 const GameBaseUrl = 'http://120.79.184.105:9003'
 const socketBaseUrl = 'ws://120.79.184.105:9004'


 // const baseUrl = ''

 // socket 地址

 export const _socketUrl = (roomId) => socketBaseUrl + '/niuniu/' + roomId

// 测试登录
export const _testLogin = () => http.GET(UserBaseUrl + '/api/testLogin/login')

// 获取登录用户
export const _getUser = () => http.GET(UserBaseUrl + '/api/login/user')

// 发送验证码
export const _getAuthCode = (phoneNum) => http.GET(UserBaseUrl + '/api/binding/phone/' + phoneNum)

// 绑定手机号
export const _bindPhone = (data) => http.POST(UserBaseUrl + '/api/front/phone/submit', data)

// 创建房卡包
export const _sendCard = (cardNum) => http.POST(GameBaseUrl + '/api/cardTrans/create/package/' + cardNum)

// 创建房间
export const _createRoom = (data) => http.PUT(GameBaseUrl + '/api/room/create/niuniu', data)



// 测试用假登陆
export const _fakeLogin = (data) => http.GET(GameBaseUrl + '/api/denglu/login/', data)
// 测试用假注册
export const _fakeRegister = (data) => http.GET(GameBaseUrl + '/api/zhuCe/login/', data)