/**
 * 
 *  confirm弹框
 * 
 * 
 */
export default class Confirm  {
    constructor (config) {
        let c = Object.assign({}, config)
        this.dom = null,
        this.title = c.title || '是否继续？'
        this.done = c.done || function () {}
        this.cancel = c.cancel || function () {}
        this.open()
    }
    open () {
        this.dom = document.createElement('div');
        this.dom.style.cssText = `
            position: fixed;
            z-index: 2019;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-color: rgba(0,0,0,0)
        `
        let box = document.createElement('div');
        box.style.cssText = `
            position: relative;
            border-radius: .14rem;
            width: 80%;
            line-height: 1rem;
            padding: .4rem;
            box-sizing: border-box;
            color: #333;
            top: 50%;
            transform: translateY(-50%);
            left: 10%;
            text-align: right;
            background-color: #fff;
            -webkit-box-shadow: 0 3px 1px -2px rgba(0,0,0,.2), 0 2px 2px 0 rgba(0,0,0,.14), 0 1px 5px 0 rgba(0,0,0,.12);
            box-shadow: 0 3px 1px -2px rgba(0,0,0,.2), 0 2px 2px 0 rgba(0,0,0,.14), 0 1px 5px 0 rgba(0,0,0,.12);
        `
        let p = document.createElement('p');
        p.innerText = this.title
        p.style.cssText = `
            font-size: .34rem;
            text-align: left;
            word-break: break-all;
        `
        let doneBtn = document.createElement('button');
        doneBtn.onclick = (e) => {
            this.submitDone(e)
        }
        doneBtn.innerText = '确定'
        doneBtn.style.cssText = `
            color: #2196F3;
            font-size: .4rem;
            outline: none;
            margin-right: .3rem;
            border: none;
            background-color: rgba(0,0,0,0);
        `
        let cancelBtn = document.createElement('button');
        cancelBtn.onclick = (e) => {
            this.submitCancel(e)
        }
        cancelBtn.innerText = '取消'
        cancelBtn.style.cssText = `
            margin-right: 1rem;
            color: #666666;
            font-size: .4rem;
            outline: none;
            border: none;
            background-color: rgba(0,0,0,0);
        `
        box.appendChild(p)
        box.appendChild(cancelBtn)
        box.appendChild(doneBtn)
        this.dom.appendChild(box)
        document.body.appendChild(this.dom)
    }
    submitDone (e) {
        this.close(e);
        this.done(e);
    }
    submitCancel (e) {
        this.close(e);
        this.cancel(e);
    }
    close () {
        document.body.removeChild(this.dom)
    }
}