/*

状态控制

*/
import MusicController from '../public/MusicController'
import Socket from './socket'
import {winw, winh} from './config'


const State = {
    // phaser对象
    phaser: null,

    // 初始化挂载phaser对象
    init: function (phaser) {
        this.phaser = phaser
    },

    // 是否是观战
    is_watcher: false,

    // 是否开始游戏
    is_start: false,

    // 游戏阶段
    // 0 => 未准备   1 => 已准备   2 => 抢庄回合  3 => 闲家下注
    stage: 0,
    stageText: '未准备',

    // 游戏连接
    url: 'https://www.baidu.com',

    // 对局
    round: {
        total: 12, // 总共
        current: 1, // 当前
        double: 1, // 低分
    },

    // 组件实例
    target: {},

    // 开始游戏
    gameStart: function () {
        let vm = this
        this.is_start = true
        let game = vm.phaser.game
        const rem = game.width / 100
        // 计分牌
        vm.target.game_board = game.add.sprite(game.width / 2, 0, 'game_board');
        vm.target.game_board.scale.setTo(.28, .25);
        vm.target.game_board.anchor.setTo(0.5, 0);
        // 对局详情
        vm.target.round_info = game.add.text(game.width / 2, 32, vm.round.current + '/' + vm.round.total + '局',  { font: "40px Arial", fill: '#333333'});
        vm.target.round_info.anchor.setTo(0.5);
        vm.target.round_double = game.add.text(game.width / 2, 78, '底分:' + vm.round.double + '分',  { font: "34px Arial", fill: '#333333'});
        vm.target.round_double.anchor.setTo(0.5);

        // 阶段提醒
        vm.target.stage = game.add.text(game.width / 2, 45 * rem, vm.stageText,  { font: "50px Arial", fill: '#FFCE44'});
        vm.target.stage.fontWeight = 'bold';
        vm.target.stage.anchor.setTo(0.5);

        vm.createPrepare()
    },

    // 生成准备按钮
    createPrepare: function () {
        let vm = this
        this.is_start = true
        let game = vm.phaser.game
        // 准备按钮
        vm.target.prepare =  game.add.sprite(game.width / 2, game.height - 370, 'niuniu_prepare');
        vm.target.prepare.anchor.setTo(0.5, 0);
        vm.target.prepare_text = game.add.text(game.width / 2, game.height - 360, '准备',  { font: "44px Arial", fill: '#ffffff'});
        vm.target.prepare_text.anchor.setTo(0.5, 0);
        vm.target.prepare.inputEnabled = true;
        vm.target.prepare.events.onInputDown.add(vm.onPrepare, vm.phaser);
    },

    // 更新对局文本
    updateRoundText: function (curr, total) {
        let vm = this
        vm.target.round_info.setText(curr + '/' + total + '局');
    },


    // 更新阶段提醒
    updateStage: function (str) {
        let vm = this
        vm.target.stage.setText(str);
    },

    // 生成庄家标识
    createMaster: function (index) {
        setTimeout(() => {
            let vm = this
            let game = vm.phaser.game
            let group = game.add.group();
            const rem = game.width / 100
            let text = `庄`
            let text2 = `家`
            let offseTop = 0
            let offseLeft = .5 * rem
            if (vm.target.master) {
                vm.target.master.destroy()
            }
            // 左边一排
            if (index <= 5) {
                offseTop =  (21 * (5 - index) * rem) + (rem * 5)
            }
            // 右边一排
            else {
                offseTop = (21 * (index - 6) * rem) + (rem * 5)
                offseLeft = game.width - 4 * rem
            }
            if (index == -1) {
                offseTop = game.height + 8 - 290
                offseLeft = 14 * rem
            }
            let z = game.add.text(offseLeft, offseTop, text,  { font: "26px Arial bold", fill: '#FFCC00'});
            let j = game.add.text(offseLeft, offseTop + 30, text2,  { font: "26px Arial bold", fill: '#FFCC00'});
            z.stroke = '#CC0000';
            z.strokeThickness = 4;
            j.stroke = '#CC0000';
            j.strokeThickness = 4;
            group.add(z)
            group.add(j)
            vm.target.master = group
        }, 30)
    },

    // 生成看牌按钮
    showmeButton: function () {
        setTimeout(() => {
            let vm = State
            let game = vm.phaser.game
            let group = game.add.group();
            let k = game.add.sprite(game.width / 2 - 150, game.height - 370, 'niuniu_prepare');
            k.anchor.setTo(0.5, 0);
            let k2 = game.add.text(game.width / 2 - 150, game.height - 360, '看牌',  { font: "44px Arial", fill: '#ffffff'});
            k2.anchor.setTo(0.5, 0);
            group.add(k)
            group.add(k2)
            k.inputEnabled = true;
            k.events.onInputDown.add(Socket.openmePoker, vm.phaser); 
            k2.inputEnabled = true;
            k2.events.onInputDown.add(Socket.openmePoker, vm.phaser); 
            vm.target.showmeButton = group
        })
    },

    // 生成摊牌按钮
    showdownButton: function () {
        setTimeout(() => {
            let vm = State
            let game = vm.phaser.game
            let group = game.add.group();
            let k = game.add.sprite(game.width / 2 + 150, game.height - 370, 'niuniu_prepare');
            k.anchor.setTo(0.5, 0);
            let k2 = game.add.text(game.width / 2 + 150, game.height - 360, '摊牌',  { font: "44px Arial", fill: '#ffffff'});
            k2.anchor.setTo(0.5, 0);
            group.add(k)
            group.add(k2)
            k.inputEnabled = true;
            k.events.onInputDown.add(Socket.openPoker, vm.phaser); 
            k2.inputEnabled = true;
            k2.events.onInputDown.add(Socket.openPoker, vm.phaser); 
            vm.target.showdownButton = group
        }, 50)
    },

    // 生成抢庄按钮
    robButton: function () {
        setTimeout(() => {
            let vm = State
            let game = vm.phaser.game
            const rem = game.width / 100
            let group = game.add.group();
            for (let i = 0; i < 4; i++) {
                let target = 'double' + (i + 1)
                let targetText = 'double' + (i + 1) + 'text'
                let text = (i + 1) + ' 倍'
                let offsetLeft = 20 * rem + 20 * i * rem
                if (i == 3) {
                    text = '不抢'
                }
                vm.target[target] =  game.add.sprite(offsetLeft, game.height - 370, 'niuniu_prepare');
                vm.target[target].anchor.setTo(0.5, 0);
                vm.target[target].scale.setTo(.8, 1);
                vm.target[targetText] = game.add.text(offsetLeft, game.height - 360, text,  { font: "44px Arial", fill: '#ffffff'});
                vm.target[targetText].anchor.setTo(0.5, 0);
                vm.target[target].inputEnabled = true;
                vm.target[target].events.onInputDown.add(Socket["rob" + i], vm.phaser); 
                group.add(vm.target[target])
                group.add(vm.target[targetText])
            }
            vm.target.robButton = group
        }, 50 )
    },

    // 生成下注按钮
    betButton: function () {
        setTimeout(() => {
            let vm = State
            let game = vm.phaser.game
            const rem = game.width / 100
            let group = game.add.group();
            for (let i = 0; i < 4; i++) {
                let target = 'doubles' + (i + 1)
                let targetText = 'doubles' + (i + 1) + 'text'
                let text = (i + 1) + ' 倍'
                let offsetLeft = 20 * rem + 20 * i * rem
                switch (i) {
                    case 0:
                        text = '1 倍'
                        break;
                    case 1:
                        text = '3 倍'
                        break;
                    case 2:
                        text = '5 倍'
                        break;
                    case 3:
                        text = '8 倍'
                        break;
                }
                vm.target[target] =  game.add.sprite(offsetLeft, game.height - 370, 'niuniu_prepare');
                vm.target[target].anchor.setTo(0.5, 0);
                vm.target[target].scale.setTo(.8, 1);
                vm.target[targetText] = game.add.text(offsetLeft, game.height - 360, text,  { font: "44px Arial", fill: '#ffffff'});
                vm.target[targetText].anchor.setTo(0.5, 0);
                vm.target[target].inputEnabled = true;
                vm.target[target].events.onInputDown.add(Socket["bet" + i], vm.phaser); 
                group.add(vm.target[target])
                group.add(vm.target[targetText])
            }
            vm.target.betButton = group
        }, 50)
    },

    // 倒计时
    countDown: function (n) {
        let vm = State
        let game = vm.phaser.game
        const rem = game.width / 100
        if (vm.target.countDown) {
            vm.target.countDown.destroy()
        }
        // 背景
        let time_bg = game.add.sprite(game.width / 2, 60 * rem, 'game_time');
        time_bg.scale.setTo(1.6, 1.6);
        time_bg.anchor.setTo(0.5, 0);
        let countdown_group = game.add.group();
        let text = game.add.text(game.width / 2, 60 * rem + 65, n + '',  { font: "80px Arial", fill: '#D15D15'});
        text.fontWeight = 'bold'
        text.anchor.setTo(0.5, 0);
        countdown_group.add(time_bg)
        countdown_group.add(text)
        vm.target.countDown = countdown_group
    },

    // 点击准备
    onPrepare: function () {
        let vm = State
        Socket.ready()
        // vm.target.prepare.destroy()
        // vm.target.prepare_text.destroy()
        // vm.target.stage.setText('已准备')
        // 清除上一局信息
        
    },

    // 玩家加减分动画
    changeScoreAni: function (index, score) {
        let vm = this
        let game = vm.phaser.game
        const w = winw / 100
        const h = winh / 100

        let offseTop = 0
        let offseLeft = 17 * w
        // 左边一排
        if (index <= 6) {
            offseTop = 1 * h + (12.4 * (6 - index) * h)
        }
        // 右边一排
        else {
            offseTop = 1 * h + (12.4 * (index - 7) * h)
            offseLeft = winw - 18 * w
        }

        // 玩家自己
        if (index == 0) {
            offseTop = 1 * h + (12.4 * (6.5 - index) * h)
            offseLeft = 14 * w
        }

        if (score < 0) {
            for (let i = 0; i < 20; i++) {
                MusicController.target['sound_jinbi'].play()
                setTimeout(() => {
                    let min1 = Math.random() * 20 - 10
                    let min2 = Math.random() * 20 - 10
                    let max1 = Math.random() * 60 - 30
                    let max2 = Math.random() * 60 - 30
                    let s = game.add.sprite(game.world.centerX + max1, game.world.centerY + max2, 'coin');
                    game.add.tween(s).from( { x: offseLeft + min1, y: offseTop + min2 }, 500, Phaser.Easing.Bounce.Out, true);
                    setTimeout(() => {
                        s.destroy()
                    }, 1000)
                }, i * 50)
            }       
        } else {
            setTimeout(() => {
                MusicController.target['sound_jinbi'].play()
                for (let i = 0; i < 20; i++) {
                    setTimeout(() => {
                        let min1 = Math.random() * 20 - 10
                        let min2 = Math.random() * 20 - 10
                        let max1 = Math.random() * 60 - 30
                        let max2 = Math.random() * 60 - 30
                        let s = game.add.sprite(game.world.centerX + max1, game.world.centerY + max2, 'coin');
                        game.add.tween(s).to( { x: offseLeft + min1, y: offseTop + min2 }, 500, Phaser.Easing.Bounce.Out, true);
                        setTimeout(() => {
                            s.destroy()
                        }, 1000)
                    }, i * 50)
                }  
            }, 900)
        }
    },


    // 开场动画
    openShow: function () {
        let vm = this
        let game = vm.phaser.game
        for (let i = 1; i <= 10; i++) {
            setTimeout(() => {
                let s = game.add.sprite(game.world.centerX, game.world.centerY, 'cheers' + i);
                s.scale.setTo(4, 4);
                s.anchor.setTo(0.5, 0);
                setTimeout(() => {
                    s.destroy()
                }, 80)
            }, 80 * i)
        }
    }
   




    
}

export default State