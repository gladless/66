import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Confirm from './components/confirm'

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

const gameList = ['p13']
router.beforeEach((to, from, next) => {
  // 如果在游戏界面跳转  需要先确认
  if (from.name.indexOf(gameList) !== -1) {
    new Confirm({
      title: '是否离开游戏？',
      done: () => {
        next()
      }
    })
    // next(false)
    return false
  }
  next()
})