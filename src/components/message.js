/**
 * 
 *  消息提示
 * 
 * 
 */
export default class Message  {
    constructor (config) {
        this.dom = null,
        this.config = {
            type: 'default',
            text: '成功！',
            time: 2000
        }
        this.colorMap = {
            'success': ['#28C76F', '#4CAF50'],
            'error': ['#EA5455', '#F44336'],
            'warning': ['#FDD835', '#ffd61f'],
            'default': ['#333', '#222'],
        }
        this.config = Object.assign(this.config, config)
        this.open()
    }
    open () {
        let c = this.config
        let colorMap = this.colorMap
        this.dom = document.createElement('div');
        this.dom.innerText = c.text
        this.dom.style.cssText = `
            position: fixed;
            z-index: 2019;
            bottom: 0;
            width: 80%;
            transform: translateY(100%);
            transition: 300ms;
            left: 10%;
            border-radius: .2rem;
            height: 1.2rem;
            line-height: 1.2rem;
            text-align: center;
            font-size: .5rem;
            color: #fff;
            background: -ms-linear-gradient(left, `+colorMap[c.type][0]+`,  `+colorMap[c.type][1]+`); 
            background:-moz-linear-gradient(left,`+colorMap[c.type][0]+`,`+colorMap[c.type][1]+`);
            background:-webkit-gradient(linear, 0% 100%, 0% 0%,from(`+colorMap[c.type][0]+`), to(`+colorMap[c.type][1]+`));
            background: -webkit-gradient(linear, 0% 100%, 0% 0%, from(`+colorMap[c.type][0]+`), to(`+colorMap[c.type][1]+`));
            background: -webkit-linear-gradient(left, `+colorMap[c.type][0]+`, `+colorMap[c.type][1]+`); 
            background: -o-linear-gradient(left, `+colorMap[c.type][0]+`, `+colorMap[c.type][1]+`);
        `
        this.dom.onclick = () => {
            this.close()
        }
        document.body.appendChild(this.dom)
        setTimeout(() => {
            this.dom.style.transform = 'translateY(0)'
        }, 0);
        setTimeout(() => {
            this.close()
        }, c.time)
    }
    close () {
        if (this.dom) {
            this.dom.style.transform = 'translateY(100%)'
            setTimeout(() => {
                document.body.removeChild(this.dom)
                this.dom = null
            }, 400)
        }
    }
}