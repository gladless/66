/**
 * 
 * 13人牛牛
 * 
 */
import ImgController from '../public/ImageController'
import MusicController from '../public/MusicController'
import UIController from '../public/UIController'
import SocketController from './socket'
import Players from './player'
import State from './state'
import PlayerController from './player'

let progressText = '' // 加载文本

 export default {
     // 初始化
     init: function () {
        // 初始化各模块
        MusicController.init(this)
        ImgController.init(this)
        UIController.init(this)
        Players.init(this)
        State.init(this)


        let game = this.game
        game.clearBeforeRender = false

        // 加载页面动画
        let prebg = game.add.image(0, 0, 'preload')
        prebg.scale.setTo(game.width / prebg.width, game.height / prebg.height);
        progressText = game.add.text(game.world.centerX, game.world.centerY + 40, '0%', {fill: '#fff', fontSize: '64px'})
        progressText.anchor = {x: 0.5, y: 0.5}

     },

      // 预加载
    preload: function () {
        let load = this.load
        // 加载图片
        ImgController.allImages.forEach(e => {
            load.image(e.name, e.src)
        })
        // 加载牌
        load.spritesheet('poker', 'static/img/game_poker.png', 51, 71, 56);
        // 加载音频
        MusicController.allMusic.forEach(e => {
            load.audio(e.name, e.src);
        })
        // 进度
        this.load.onFileComplete.add((progress) => {
            progressText.text = '已加载 ' + progress + '%'
        })
    },

    // 已加载
    create: function () {

        // 初始化 socket
        SocketController.init();

         // 加载背景图
        ImgController.createBgImg();

        // 加载用户
        Players.refresh()

        // 加载音效
        MusicController.loadAllMusic()

        // 加载UI
        UIController.createBottomTab()

        // 开始游戏
        State.gameStart()

        // setTimeout(() => {
        //     State.showmeButton()
        // }, 1000)
        
    },

    // 更新
    update: function () {
        
    }  
 }