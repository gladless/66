/*

图片控制

*/

const ImgController = {
    // phaser对象
    phaser: null,

    // 初始化挂载phaser对象
    init: function (phaser) {
        this.phaser = phaser
    },

    // 所有图片
    allImages: [
        {name: 'bg', src: 'static/img/game-bg01.jpg'},
        {name: 'tab_bottom', src: 'static/img/tab_bottom.png'},
        {name: 'game_chat', src: 'static/img/game-chat.png'},
        {name: 'game_music1', src: 'static/img/game-music1.png'},
        {name: 'game_music2', src: 'static/img/game-music2.png'},
        {name: 'niuniu_prepare', src: 'static/img/niuniu-prepare.png'},
        {name: 'game_mask', src: 'static/img/game_mask.png'},
        {name: 'voices_bg', src: 'static/img/game_voices.png'},
        {name: 'advatar_null', src: 'static/img/advatar_null.png'},
        {name: 'advatar_user', src: 'static/img/advatar_user.png'},
        {name: 'advatar_default', src: 'static/img/advatar_default.jpg'},
        {name: 'advatar_offline', src: 'static/img/advatar_offline.jpg'},
        {name: 'game_board', src: 'static/img/game_board.png'},
        {name: 'game_time', src: 'static/img/game-time.png'},
        {name: 'return_index', src: 'static/img/return-index.png'},
        {name: 'game_rules', src: 'static/img/game-rules.png'},
        {name: 'sidelines', src: 'static/img/sidelines.png'},
        {name: 'showdown', src: 'static/img/niuniu-showdown.png'},
        {name: 'coin', src: 'static/img/coin.png'},
        // 牌
        {name: 'c_0', src: 'static/img/pokers/c_0.png'},
        {name: 'c_1', src: 'static/img/pokers/c_1.png'},
        {name: 'c_2', src: 'static/img/pokers/c_2.png'},
        {name: 'c_3', src: 'static/img/pokers/c_3.png'},
        {name: 'c_4', src: 'static/img/pokers/c_4.png'},
        {name: 'c_5', src: 'static/img/pokers/c_5.png'},
        {name: 'c_6', src: 'static/img/pokers/c_6.png'},
        {name: 'c_7', src: 'static/img/pokers/c_7.png'},
        {name: 'c_8', src: 'static/img/pokers/c_8.png'},
        {name: 'c_9', src: 'static/img/pokers/c_9.png'},
        {name: 'c_10', src: 'static/img/pokers/c_10.png'},
        {name: 'c_11', src: 'static/img/pokers/c_11.png'},
        {name: 'c_12', src: 'static/img/pokers/c_12.png'},
        {name: 'c_13', src: 'static/img/pokers/c_13.png'},
        {name: 'c_14', src: 'static/img/pokers/c_14.png'},
        {name: 'c_15', src: 'static/img/pokers/c_15.png'},
        {name: 'c_16', src: 'static/img/pokers/c_16.png'},
        {name: 'c_17', src: 'static/img/pokers/c_17.png'},
        {name: 'c_18', src: 'static/img/pokers/c_18.png'},
        {name: 'c_19', src: 'static/img/pokers/c_19.png'},
        {name: 'c_20', src: 'static/img/pokers/c_20.png'},
        {name: 'c_21', src: 'static/img/pokers/c_21.png'},
        {name: 'c_22', src: 'static/img/pokers/c_22.png'},
        {name: 'c_23', src: 'static/img/pokers/c_23.png'},
        {name: 'c_24', src: 'static/img/pokers/c_24.png'},
        {name: 'c_25', src: 'static/img/pokers/c_25.png'},
        {name: 'c_26', src: 'static/img/pokers/c_26.png'},
        {name: 'c_27', src: 'static/img/pokers/c_27.png'},
        {name: 'c_28', src: 'static/img/pokers/c_28.png'},
        {name: 'c_29', src: 'static/img/pokers/c_29.png'},
        {name: 'c_30', src: 'static/img/pokers/c_30.png'},
        {name: 'c_31', src: 'static/img/pokers/c_31.png'},
        {name: 'c_32', src: 'static/img/pokers/c_32.png'},
        {name: 'c_33', src: 'static/img/pokers/c_33.png'},
        {name: 'c_34', src: 'static/img/pokers/c_34.png'},
        {name: 'c_35', src: 'static/img/pokers/c_35.png'},
        {name: 'c_36', src: 'static/img/pokers/c_36.png'},
        {name: 'c_37', src: 'static/img/pokers/c_37.png'},
        {name: 'c_38', src: 'static/img/pokers/c_38.png'},
        {name: 'c_39', src: 'static/img/pokers/c_39.png'},
        {name: 'c_40', src: 'static/img/pokers/c_40.png'},
        {name: 'c_41', src: 'static/img/pokers/c_41.png'},
        {name: 'c_42', src: 'static/img/pokers/c_42.png'},
        {name: 'c_43', src: 'static/img/pokers/c_43.png'},
        {name: 'c_44', src: 'static/img/pokers/c_44.png'},
        {name: 'c_45', src: 'static/img/pokers/c_45.png'},
        {name: 'c_46', src: 'static/img/pokers/c_46.png'},
        {name: 'c_47', src: 'static/img/pokers/c_47.png'},
        {name: 'c_48', src: 'static/img/pokers/c_48.png'},
        {name: 'c_49', src: 'static/img/pokers/c_49.png'},
        {name: 'c_50', src: 'static/img/pokers/c_50.png'},
        {name: 'c_51', src: 'static/img/pokers/c_51.png'},
        {name: 'c_52', src: 'static/img/pokers/c_52.png'},
        {name: 'c_53', src: 'static/img/pokers/c_53.png'},
        {name: 'c_54', src: 'static/img/pokers/c_54.png'},
        {name: 'c_55', src: 'static/img/pokers/c_55.png'},
        // 牌结束

        // 干杯动画
        {name: 'cheers1', src: 'static/img/cheers1.png'},
        {name: 'cheers2', src: 'static/img/cheers2.png'},
        {name: 'cheers3', src: 'static/img/cheers3.png'},
        {name: 'cheers4', src: 'static/img/cheers4.png'},
        {name: 'cheers5', src: 'static/img/cheers5.png'},
        {name: 'cheers6', src: 'static/img/cheers6.png'},
        {name: 'cheers7', src: 'static/img/cheers7.png'},
        {name: 'cheers8', src: 'static/img/cheers8.png'},
        {name: 'cheers9', src: 'static/img/cheers9.png'},
        {name: 'cheers10', src: 'static/img/cheers10.png'},
    ],

    // 牌
    poker: null,
    /*
    *   game.add.sprite(left, offseTop, 'poker', val);
    *   
    *   红    A(0)    2(4)    3(8)     4(12)    5(16)    6(20)    7(24)    8(28)    9(32)    10(36)    J(40)    Q(44)    K(48)    大(52)
    *   方    A(1)    2(5)    3(9)     4(13)    517()    6(21)    7(25)    8(29)    9(33)    10(37)    J(41)    Q(45)    K(49)    小(53)
    *   黑    A(2)    2(6)    3(10)    4(14)    5(18)    6(22)    7(26)    8(30)    9(34)    10(38)    J(42)    Q(46)    K(50)    花(54)
    *   花    A(3)    2(7)    3(11)    4(15)    5(19)    6(23)    7(27)    8(31)    9(35)    10(39)    J(43)    Q(47)    K(51)    花(55)
    * 
    */

    // 图片实例
    target: {},

    // 加载背景图
    createBgImg: function () {
        let vm = this
        let game = vm.phaser.game
        // 背景缩放铺满
        let bg = game.add.sprite(0, 0, 'bg');
        bg.scale.setTo(game.width / bg.width, game.height / bg.height);
        vm.target.bg = bg
    }


}

export default ImgController;