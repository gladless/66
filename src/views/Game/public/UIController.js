/*

UI控制

*/
import MusicController from './MusicController'
import VueRouter from '../../../router'

const UIController = {
    // phaser对象
    phaser: null,

    // 初始化挂载phaser对象
    init: function (phaser) {
        this.phaser = phaser
    },

    // 语音音效列表
    voiceList: [],

    // 组件实例
    target: {},

    // 生成底部控制区
    createBottomTab: function () {
        let vm = this
        let game = vm.phaser.game
        vm.target.tab_bottom = game.add.sprite(0, game.height - 110, 'tab_bottom');
        vm.target.tab_bottom.scale.setTo(game.width / vm.target.tab_bottom.width, 1)

        // 观战
        vm.target.sidelines = game.add.sprite(game.width - 500, game.height - 80, 'sidelines');
        vm.target.sidelines.inputEnabled = true;

        // 规则
        vm.target.game_rules = game.add.sprite(game.width - 400, game.height - 80, 'game_rules');
        vm.target.game_rules.inputEnabled = true;

        // 返回首页
        vm.target.return_index = game.add.sprite(game.width - 300, game.height - 80, 'return_index');
        vm.target.return_index.inputEnabled = true;
        vm.target.return_index.events.onInputDown.add(vm.backHomePage, vm.phaser); 

        // 声音开关
        vm.target.game_music1 = game.add.sprite(game.width - 200, game.height - 80, 'game_music1');
        vm.target.game_music1.inputEnabled = true;
        vm.target.game_music1.events.onInputDown.add(vm.onSoundSwitchTap, vm.phaser); 
    
        // 音效按钮
        vm.target.game_chat = game.add.sprite(game.width - 100, game.height - 80, 'game_chat');
        vm.target.game_chat.inputEnabled = true;
        vm.target.game_chat.events.onInputDown.add(vm.openVoicesListMask, vm.phaser); 
    },

    // 返回首页
    backHomePage: function () {
        VueRouter.push({name: 'home'})
    },

    // 当点击音效开关 this => phaser
    onSoundSwitchTap: function () {
        let vm = UIController
        if (MusicController.play) {
            MusicController.target.bg_music.pause();
            vm.target.game_music1.loadTexture('game_music2');
        } else {
            MusicController.target.bg_music.resume();
            vm.target.game_music1.loadTexture('game_music1');
        }
        MusicController.play = !MusicController.play;
    },

    // 打开音效列表遮罩 this => phaser
    openVoicesListMask: function () {
        let game = this.game
        let vm = UIController
        vm.target.voice_group = game.add.group();

        // 音效选择遮罩
        let mask = game.add.sprite(0, 0, 'game_mask');
        mask.scale.setTo(game.width / mask.width, game.height / mask.height);
        mask.inputEnabled = true;
        mask.events.onInputDown.add(vm.closeVouceListMask, this); 
        vm.target.voice_group.add(mask)

        // 音效选择背景
        let voices_bg = game.add.sprite(game.width - 460, game.height - 960, 'voices_bg');
        voices_bg.scale.setTo(10 / 7, 10 / 7);  //  width 350  *  10/7
        vm.target.voice_group.add(voices_bg);

        // 音效列表
        let arr = []
        MusicController.allMusic.forEach(e => {
            if (e.text) {
                arr.push(e)
            }
        })
        for (let i = 0; i < arr.length; i++) {
            let text = game.add.text(game.width - 208, game.height - 810 + i * 68, arr[i].text,  { font: "30px Arial", fill: '#ffffff'});
            text.anchor.setTo(0.5);
            text.inputEnabled = true;
            text.events.onInputDown.add(vm.handleChooseVioce, this); 
            vm.target.voice_group.add(text)
        }
        vm.voiceList = arr

        // 进场动画
        game.add.tween(vm.target.voice_group).from( { x: 350 * 10/7 }, 300, Phaser.Easing.Exponential.Out, true);
    },

    // 关闭音效列表遮罩 this => phaser
    closeVouceListMask: function () {
        let vm = UIController
        let game = vm.phaser.game
        game.add.tween(vm.target.voice_group).to( { x: game.world.centerX }, 300, Phaser.Easing.Exponential.Out, true);
        setTimeout(() => {
            vm.target.voice_group.destroy()
        }, 300)
    },

    // 点击一条音效 this => phaser
    handleChooseVioce: function (pointer) {
        let vm = UIController
        let game = vm.phaser.game
        const rem = game.width / 100
        vm.closeVouceListMask()
        if (!MusicController.play) return // 如果是静音状态则不播放音频
        let target = vm.voiceList.find(e => e.text === pointer.text)
        if (target) {
            MusicController.target[target.name].play()
            // 文本提示
            let text = game.add.text(17 * rem, game.height - 280, pointer.text);
            text.fontSize = 30;
            text.fontWeight = 'bold';
            // 设置文本描边
            text.stroke = '#000000';
            text.strokeThickness = 6;
            text.fill = '#ffffff';
            game.add.tween(text).from( { width: 0, height: 0 }, 500, Phaser.Easing.Bounce.Out, true);
            // 销毁
            setTimeout(() => {
                text.destroy();
            }, 2000)
        }
    },

    // 其他玩家点击音效
    // player: Number 玩家
    // voice: Number  音效
    otherChooseVoice: function (player = 0, voice = 0) {
        let vm = UIController
        let game = vm.phaser.game
        const rem = game.width / 100
        if (!MusicController.play) return // 如果是静音状态则不播放音频
        let target = MusicController.allMusic.find(e => e.value === voice)
        MusicController.target[target.name].play()

        // 文本提示
        let left = 17 * rem
        let offseTop = 0
        // 左边一排
        if (player <= 5) {
            offseTop = 1 * rem + (21 * (5 - player) * rem) + 10
        }
        // 右边一排
        else {
            offseTop = 1 * rem + (21 * (player - 6) * rem) + 10
            left = game.width - 17 * rem
        }
        let text = game.add.text(left, offseTop, target.text);
        if (player > 5) {
            text.anchor.set(1, 0);
        }
        text.fontSize = 30;
        text.fontWeight = 'bold';
        // 设置文本描边
        text.stroke = '#000000';
        text.strokeThickness = 6;
        text.fill = '#ffffff';
        game.add.tween(text).from( { width: 0, height: 0 }, 500, Phaser.Easing.Bounce.Out, true);
        // 销毁
        setTimeout(() => {
            text.destroy();
        }, 2000)
    }
}

export default UIController;